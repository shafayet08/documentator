from django.urls import path
from . import views

app_name = 'docapps'
urlpatterns = [
    path('', views.dashboard, name='dashboard'),
    path('expiring_soon_detail_page/', 
            views.expiring_soon_detail_page, 
            name='expiring_soon_detail_page'),
    path('expired/', views.expired, name='expired'),

    path('email_settings/', views.email_settings, name='email_settings'),
    path('send_email/', views.send_email, name='send_email'),

    path('create_user/', views.create_user, name='create_user'),
    path('user_info/', views.user_info, name='user_info'),
    path('edit_user/<int:pk>/', views.edit_user, name='edit_user'),

    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),


    




]
