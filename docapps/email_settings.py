from django.core.mail.backends.smtp import EmailBackend

from . models import Email

class mail_setting:
    config = Email.objects.first()
    backend = EmailBackend(host=config.email_host, 
                            port=config.email_port, 
                            username=config.email_host_user, 
                            password=config.email_host_password, 
                            use_tls=config.email_use_tls, 
                            fail_silently=False)

    # def() print('hello i am working')