from django.contrib import admin

from . models import Document, Email, Test
# Register your models here.
admin.site.register(Document)
admin.site.register(Email)
admin.site.register(Test)