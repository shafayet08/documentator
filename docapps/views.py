from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User

from django.core.mail import EmailMessage
from django.contrib import messages

from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.admin.views.decorators import staff_member_required

from . forms import ModelDocumentForm, CreateUserForm, EditUserForm
from . models import Document


# Create your views here.
@login_required(redirect_field_name='docapps:login')
def dashboard(request):
    docs = Document.objects.all()
    context = {'docs': docs,}
    if request.method == 'POST':
        print(request.POST)
        form = ModelDocumentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            form = ModelDocumentForm()
            return redirect('docapps:dashboard')
        else:
            print(form.errors)

    return render(request, 'docapps/dashboard.html', context)

@login_required(redirect_field_name='docapps:login')
def expiring_soon_detail_page(request):
    docs = Document.objects.filter(is_expiring_soon=True)
    return render(request, 'docapps/expiring_soon_detail_page.html', {'docs': docs})

@login_required(redirect_field_name='docapps:login')
def expired(request):
    docs = Document.objects.filter(is_expired=True)
    return render(request, 'docapps/expired.html', {'docs': docs})

@login_required(redirect_field_name='docapps:login')
def email_settings(request):
    if request.method == 'POST':
        email.email_host = request.POST['email_host']
        email.email_port =request.POST['email_port']
        email.email_host_user = request.POST['email_host_user']
        email.email_host_password = request.POST['email_host_password']
        email.email_use_tls = request.POST['email_use_tls']
        email.email_default_name = request.POST['email_default_name']
        email.save()
        messages.success(request, f'User has been successfully updated')
        return render(request, 'docapps/email_settings.html')

    return render(request, 'docapps/email_settings.html')

from django.core.mail import send_mail
def send_email(request):
    send_mail('Title', 'Body',
    'shafayetbs@gmail.com',
    ['haydarbsss@gmail.com'],
    fail_silently=False,
    )
    return HttpResponse('Email sent!')

@login_required(redirect_field_name='docapps:login')
@user_passes_test(lambda u: u.is_superuser)
def create_user(request):
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
        else:
            print(form.errors)
            username = form.cleaned_data.get('username')
            messages.success(request, f'Accounts has been successfully created for {username} ')
            return render(request, 'docapps/createForm.html')
    return render(request, 'docapps/createForm.html')

@login_required(redirect_field_name='docapps:login')
def user_info(request):
    users = User.objects.all()
    return render(request, 'docapps/userList.html', {'users': users})

@login_required(redirect_field_name='docapps:login')
@user_passes_test(lambda u: u.is_superuser)
def edit_user(request, pk):
    user = User.objects.get(id=pk)
    form = EditUserForm(instance=user)

    if request.method == 'POST':
        print(request.POST)
        form = EditUserForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            messages.success(request, f'User has been successfully updated')
            return redirect('docapps:user_info')

    return render(request, 'docapps/userPermission.html', {'form': form})

def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            auth_login(request, user)
            print('login success!')
            messages.success(request, f'Hey {username}! You have been logged in.')
            return redirect('docapps:dashboard')
        else:
            return render(request, 'docapps/login.html')
    return render(request, 'docapps/login.html')

def logout(request):
    auth_logout(request)
    messages.success(request, f'Hey! You have been logged out.')
    return redirect('docapps:login')