import datetime

from . models import Document
from . email_settings import mail_setting

from django.core.mail import EmailMessage
from datetime import datetime

def my_scheduled_job():
    docs = Document.objects.filter(is_expired=False)
    today = datetime.today().strftime("%Y-%m-%d")
    for doc in docs:
        expired = doc.expiring_date.strftime("%Y-%m-%d")
        if expired < today:
            doc.is_expired = True
            doc.save()

            obj = mail_setting()
            p = obj.config

            sub = 'Document Expired!'
            body = 'Your document has been expired. Contact admin to add your documents again.'
            from_mail = p.email_host
            to = doc.email
            connection = obj.backend

            email = EmailMessage(subject=sub, 
                                    body=body, 
                                    from_email=from_mail, 
                                    to=[to], 
                                    connection=connection)
            email.send()


# def my_scheduled_job():
#     docs = Document.objects.filter(is_expired=False)
#     today = datetime.today().strftime("%Y-%m-%d")
#     for doc in docs:
#         expired = doc.expiring_date.strftime("%Y-%m-%d")
#         if expired < today:
#             doc.is_expired = True
#             doc.save()
#             send_mail('Title', 'Body',
#             'shafayetbs@gmail.com',
#             ['haydarbsss@gmail.com'],
#             fail_silently=False,
#             )


# def notification(request):
#     all_expired_docs = Document.objects.filter(is_expired=False)
#     today = (datetime.date.today()).strftime("%Y-%m-%d")
#     for expired_doc in all_expired_docs:
#         expired = (expired_doc.expiring_date).strftime("%Y-%m-%d")
#         if expired < today:
#             expired_doc.is_expired = True
#             expired_doc.save()
