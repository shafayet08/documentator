import datetime
from datetime import timedelta

from . models import Document

def notification(request):
    # all_expired_docs = Document.objects.filter(is_expired=False)
    # today = (datetime.date.today()).strftime("%Y-%m-%d")
    # for expired_doc in all_expired_docs:
    #     # today = datetime.today().strftime('%d-%m-%Y')
    #     expired = (expired_doc.expiring_date).strftime("%Y-%m-%d")
    #     if expired < today:
    #         expired_doc.is_expired = True
    #         expired_doc.save()



    docs = Document.objects.all()
    for doc in docs:
        today = datetime.date.today()
        five_days_ahead = today + timedelta(days=5)
        expiring_date = doc.expiring_date
        if expiring_date < five_days_ahead and doc.is_expired == False:
            doc.is_expiring_soon = True
            doc.save()
        if doc.is_expired == True and doc.is_expiring_soon == True:
            doc.is_expiring_soon = False
            doc.save()

    expired_notification = Document.objects.filter(is_expired=True).count()
    expiring_soon_notification = Document.objects.filter(is_expiring_soon=True).count()
    all_notifications = expiring_soon_notification + expired_notification

    return {'expired_notification':expired_notification,
            'expiring_soon_notification': expiring_soon_notification,
            'all_notifications': all_notifications
    }