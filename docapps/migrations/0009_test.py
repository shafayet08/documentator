# Generated by Django 3.1.7 on 2021-03-06 18:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('docapps', '0008_auto_20210306_1540'),
    ]

    operations = [
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
            ],
        ),
    ]
