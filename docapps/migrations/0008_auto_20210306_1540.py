# Generated by Django 3.1.7 on 2021-03-06 15:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('docapps', '0007_auto_20210302_0356'),
    ]

    operations = [
        migrations.RenameField(
            model_name='document',
            old_name='is_five_days_ahead',
            new_name='is_expiring_soon',
        ),
    ]
