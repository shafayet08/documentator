from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from . models import Document, Email

class ModelDocumentForm(ModelForm):
    class Meta:
        model = Document
        fields = ['doc_title', 'doc_file', 'expiring_date', 'email', 'phone']

class ModelEmailForm(ModelForm):
    class Meta:
        model = Email
        fields = ['email_host', 'email_port', 'email_host_user', 'email_host_password', 'email_use_tls', 'email_default_name']

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

class EditUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'is_staff', 'is_active', 'is_superuser', 'date_joined', 'last_login' ]