from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.fields import CharField

# Create your models here.
class Document(models.Model):
    doc_title = models.CharField(max_length=100)
    doc_file = models.FileField()
    email = models.EmailField()
    phone = models.CharField(max_length=11, blank=True)
    expiring_date = models.DateField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    is_expired = models.BooleanField(default=False)
    is_expiring_soon = models.BooleanField(default=False)
    

    class Meta:
        ordering = ['-created']

    def __str__(self):
        if self.expiring_date != None:
            created = self.created.strftime('%d-%m-%Y')
            expired = self.expiring_date.strftime('%d-%m-%Y')
        created = self.created
        expired = self.expiring_date
        return f'{self.doc_title}-created at {created} - expired at {expired}'

class Email(models.Model):
    email_host = models.CharField(max_length=50)
    email_port = models.CharField(max_length=3)
    email_host_user = models.CharField(max_length=50)
    email_host_password = models.CharField(max_length=100)
    email_use_tls = models.BooleanField(default=False)
    email_default_name = models.CharField(max_length=50)   

class Test(models.Model):
    name = models.CharField(max_length=20)