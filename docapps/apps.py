from django.apps import AppConfig


class DocappsConfig(AppConfig):
    name = 'docapps'
